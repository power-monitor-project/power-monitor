#include <std.hpp>
#include <helper.hpp>
#include <MQTTPublisher.hpp>
#include <SoftwareSerial.h>

#ifndef DEBUG
//#define DEBUG
#endif

/*    GLOBALS   */
MQTTPublisher *mqtt_publisher;
SoftwareSerial sw_port_household;
SoftwareSerial sw_port_heating;
ring_buffer_t RX_buffer_household;
ring_buffer_t RX_buffer_heating;

void setup()
{
    chip_setup();
    network_setup();
    ota_setup();

    mqtt_publisher = new MQTTPublisher(mqtt_server, mqtt_port);

    sw_port_household.begin(SW_BAUD_RATE, SWSERIAL_8N1, SW_RX_HOUSEHOLD, -1, false);
    if (!sw_port_household)
    {
        Serial.println("[SW Serial] invalid pin configuration for household.");
        while (1)
        {
        }
    }
    sw_port_heating.begin(SW_BAUD_RATE, SWSERIAL_8N1, SW_RX_HEATING, -1, false);
    if (!sw_port_household)
    {
        Serial.println("[SW Serial] invalid pin configuration for household.");
        while (1)
        {
        }
    }

    ring_buffer_init(&RX_buffer_household);
    ring_buffer_init(&RX_buffer_heating);
    delay(2000);
}

void loop()
{
    // add data from software serial port to ring buffer
    while (sw_port_household.available() > 0)
    {
        ring_buffer_queue(&RX_buffer_household, sw_port_household.read());
        if (sw_port_household.overflow())
        {
#ifdef DEBUG
            char tmp_msg[64];
            snprintf(tmp_msg, 64, "[SW Serial] overflow");
            Serial.println(tmp_msg);
#endif
        }
    }

    while (sw_port_heating.available() > 0)
    {
        ring_buffer_queue(&RX_buffer_heating, sw_port_heating.read());
        if (sw_port_heating.overflow())
        {
#ifdef DEBUG
            char tmp_msg[64];
            snprintf(tmp_msg, 64, "[SW Serial] overflow");
            Serial.println(tmp_msg);
#endif
        }
    }

    sml_data_t decoded_data;
    if (try_decode_sml(&RX_buffer_household, &decoded_data))
    {
        mqtt_publisher->publish_household(decoded_data.current_W, 
            decoded_data.overall_Wh);
    }
    if (try_decode_sml(&RX_buffer_heating, &decoded_data))
    {
        mqtt_publisher->publish_heating(decoded_data.current_W, 
            decoded_data.day_Wh, decoded_data.night_Wh, decoded_data.overall_Wh);
    }
    
    mqtt_publisher->loop();
    ArduinoOTA.handle();
    yield();
}
