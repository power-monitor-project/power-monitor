#include <MQTTPublisher.hpp>

MQTTPublisher::MQTTPublisher(String mqtt_server, uint16_t mqtt_server_port)
{
    this->mqtt_server = mqtt_server;
    this->mqtt_server_port = mqtt_server_port;

    Serial.print("[MQTT] setting up...");

    this->wifi_client = new WiFiClientSecure();
    this->pub_sub_client = new PubSubClient(*(this->wifi_client));
    this->pub_sub_client->setServer(this->mqtt_server.c_str(),
                                    this->mqtt_server_port);
    Serial.println(" completed.");

    load_certs(this->wifi_client);
    delay(200);
    this->connect();
}

MQTTPublisher::~MQTTPublisher(void)
{
    delete this->pub_sub_client;
}

void MQTTPublisher::connect(void)
{
    // loop until we're reconnected
    while (!pub_sub_client->connected())
    {
        Serial.println("[MQTT] attempting to connect...");
        if (pub_sub_client->connect(host_name))
        {
            Serial.println("[MQTT] connected, publish heartbeat...");

            // once connected, publish an announcement...
            char tmp_msg[64];
            snprintf(tmp_msg, 64, "[MQTT] connected, current FW %s", AUTO_VERSION);
            this->publish_debug(tmp_msg, 32);
        }
        else
        {
            Serial.print("[MQTT] connection failed, ");
            Serial.println(pub_sub_client->state());
            Serial.println("[MQTT] trying to establish connection again...");
        }
        delay(2000);
    }
}

void MQTTPublisher::publish_household(float curr_W, float overall_Wh)
{
    StaticJsonDocument<JSON_BUFFER> JSON_doc;

    JSON_doc["Client"] = host_name;
    JSON_doc["FW"] = AUTO_VERSION;
    JSON_doc["IP"] = eth_client->localIP();
    JSON_doc["W"] = curr_W;
    JSON_doc["Total Wh"] = overall_Wh;

    char tmp_buffer[JSON_BUFFER];
    serializeJson(JSON_doc, tmp_buffer);

    if (!this->pub_sub_client->connected())
    {
        this->connect();
    }
    else
    {
        this->pub_sub_client->publish(mqtt_topic_household, tmp_buffer);
    }
}

void MQTTPublisher::publish_heating(float curr_W, float day_Wh, float night_Wh, float overall_Wh)
{
    StaticJsonDocument<JSON_BUFFER> JSON_doc;

    JSON_doc["Client"] = host_name;
    JSON_doc["FW"] = AUTO_VERSION;
    JSON_doc["IP"] = eth_client->localIP();
    JSON_doc["W"] = curr_W;
    JSON_doc["Day Wh"] = day_Wh;
    JSON_doc["Night Wh"] = night_Wh;
    JSON_doc["Total Wh"] = overall_Wh;

    char tmp_buffer[JSON_BUFFER];
    serializeJson(JSON_doc, tmp_buffer);

    if (!this->pub_sub_client->connected())
    {
        this->connect();
    }
    else
    {
        this->pub_sub_client->publish(mqtt_topic_heating, tmp_buffer);
    }
}

void MQTTPublisher::publish_debug(char *msg, uint16 len)
{
    StaticJsonDocument<JSON_BUFFER> JSON_doc;

    JSON_doc["Client"] = host_name;
    JSON_doc["FW"] = AUTO_VERSION;
    JSON_doc["IP"] = eth_client->localIP();
    JSON_doc["Msg"] = String(msg);
    char tmp_buffer[JSON_BUFFER];
    serializeJson(JSON_doc, tmp_buffer);

    if (!this->pub_sub_client->connected())
    {
        this->connect();
    }
    else
    {
        this->pub_sub_client->publish(mqtt_topic_debug, tmp_buffer);
    }
}

void MQTTPublisher::loop(void)
{
    this->pub_sub_client->loop();
}
