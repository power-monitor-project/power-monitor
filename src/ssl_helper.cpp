#include <ssl_helper.hpp>

void load_certs(WiFiClientSecure *wifi_client)
{
    Serial.println("[TLS] loading certificates from flash...");
    if (!LittleFS.begin())
    {
        Serial.println("[LittleFS] error mouting filesystem.");
        return;
    }

    // loading client certificates
    File client_cert_file = File(LittleFS.open("/client_cert.der", "r"));
    if (!client_cert_file)
    {
        Serial.println("[LittleFS] error opening client certificate.");
        return;
    }
    else
    {
        Serial.println("[LittleFS] successfully opened client certificate.");
    }

    // load client private key
    File client_key_file = File(LittleFS.open("/client_key.der", "r"));
    if (!client_key_file)
    {
        Serial.println("[LittleFS] error opening client private key.");
        return;
    }
    else
    {
        Serial.println("[LittleFS] successfully opened client private key.");
    }

    // IMPORTANT: do not deallocate pointers (no copy in SSL lib!)
    X509List *client_cert = new X509List(client_cert_file, client_cert_file.size());
    PrivateKey *client_key = new PrivateKey(client_key_file, client_key_file.size());
    wifi_client->setClientRSACert(client_cert, client_key);
    delay(500);

    // load CA
    File ca_file = File(LittleFS.open("/ca.der", "r"));
    if (!ca_file)
    {
        Serial.println("[LittleFS] error opening CA.");
        return;
    }
    else
    {
        Serial.println("[LittleFS] successfully opened CA.");
    }

    X509List *ca_cert = new X509List(ca_file, ca_file.size());
    wifi_client->setTrustAnchors(ca_cert);

    char err_buf[100];
    if (wifi_client->getLastSSLError(err_buf, 100) < 0) {
          Serial.println(err_buf);
    }

    LittleFS.end();
}

void get_ntp_time(void)
{
    Serial.print("[NTP] synchronizing time.");
    configTime(TZ, NTP_SERVER);

    time_t now = time(nullptr);
    while (now < 1000)
    {
        delay(500);
        Serial.print(".");
        now = time(nullptr);
    }
    Serial.println();

    struct tm time_info;
    gmtime_r(&now, &time_info);

    Serial.print("[NTP] current time: ");
    Serial.print(asctime(&time_info));
}
