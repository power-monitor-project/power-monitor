#include <helper.hpp>

Wiznet5500lwIP *eth_client;
PublicKey *ota_public_key;
HashSHA256 *ota_hash;
SigningVerifier *ota_signing_verifier;

void chip_setup(void)
{
    // boot up
    Serial.begin(115200);
    delay(10000);
    Serial.println("\nPOWER MONITOR - BOOT\n");
}

void network_setup(void)
{
    // set SPI-CS pin (not the "normal one"),
    // see: https://esp8266hints.wordpress.com/2018/02/13/adding-an-ethernet-port-to-your-esp-revisited/
    SPI.begin();
    SPI.setBitOrder(MSBFIRST);
    SPI.setDataMode(SPI_MODE0);
    SPI.setFrequency(4000000);

    WiFi.mode(WIFI_OFF);

    eth_client = new Wiznet5500lwIP(ETH_CS_PIN);
    eth_client->begin(mac_addr);
    eth_client->setHostname(host_name);
    eth_client->setDefault();

    Serial.print("[Ethernet] connecting.");
    while (!eth_client->connected())
    {
        Serial.print(".");
        delay(1000);
    }
    Serial.println();

    Serial.print("[Ethernet] connection established at ");
    Serial.println(eth_client->localIP());

    Serial.print("[Ethernet] subnet mask: ");
    Serial.println(eth_client->subnetMask());
    Serial.print("[Ethernet] gateway: ");
    Serial.println(eth_client->gatewayIP());

    get_ntp_time();

    // setup OTA
    ArduinoOTA.setHostname(mDNS_name);
    ArduinoOTA.setPassword(ota_pass);
    ArduinoOTA.begin();
}

int try_decode_sml(ring_buffer_t *RX_buffer, sml_data_t *result)
{
    // check whether data can be decoded
    uint16 first_ptr = 0;
    uint16 second_ptr = 0;
    bool esc_seq_fir = FALSE;
    bool esc_seq_sec = FALSE;
    char current_byte = 0x00;
    uint32 peeked_seq = 0x00000000;
    FastCRC16 CRC16;
    for (uint16 i = 0; i < ring_buffer_num_items(RX_buffer); i++)
    {
        ring_buffer_peek(RX_buffer, &current_byte, i);

        // shift peeked_seq
        peeked_seq = peeked_seq << 8;
        peeked_seq |= (0xFFFFFFFF & current_byte);

        if (peeked_seq == SML_ESCAPE_SEQ && (esc_seq_fir & esc_seq_sec) &&
            (second_ptr + 4) < ring_buffer_num_items(RX_buffer))
        {
            // transfer message into buffer
            char tmp_buffer[RING_BUFFER_SIZE];
            ring_buffer_size_t buf_len = ring_buffer_dequeue_arr(RX_buffer,
                                                                 tmp_buffer, RING_BUFFER_SIZE);

            // check CRC checksum
            uint16 data_len = (second_ptr + 2) - (first_ptr - 3) + 1; // (end) - (begin) + 1
            uint16 crc16 = CRC16.x25((uint8_t *)&tmp_buffer[first_ptr - 3], data_len);
            // attention: bytes of datagram are interchanged with each other
            uint16 crc16_given = tmp_buffer[second_ptr + 4] << 8 | tmp_buffer[second_ptr + 3];
            if (crc16_given != crc16)
            {
                return 0;
            }

            // decode message
            uint16 actual_ptr = 8 + (first_ptr - 3);
            sml_file *file = sml_file_parse((unsigned char *)tmp_buffer + actual_ptr, buf_len);

            for (uint8 i = 0; i < file->messages_len; i++)
            {
                sml_message *message = file->messages[i];

                // LIST RESPONSE
                if (*message->message_body->tag == SML_MESSAGE_GET_LIST_RESPONSE)
                {
                    sml_list *entry;
                    sml_get_list_response *body;
                    body = (sml_get_list_response *)message->message_body->data;

                    // further decode
                    float overall_Wh = 0;
                    float day_Wh = 0;
                    float night_Wh = 0;
                    float curr_W = 0;

                    char tmp_msg[128];
                    snprintf(tmp_msg, 128, "[SML] datagram received");
                    Serial.println(tmp_msg);

                    for (entry = body->val_list; entry != NULL; entry = entry->next)
                    {
                        if (entry->value->type == SML_TYPE_OCTET_STRING)
                        {
                            char *str;
                            snprintf(tmp_msg, 128, "%d-%d:%d.%d.%d*%d#%s#\n",
                                     entry->obj_name->str[0], entry->obj_name->str[1],
                                     entry->obj_name->str[2], entry->obj_name->str[3],
                                     entry->obj_name->str[4], entry->obj_name->str[5],
                                     sml_value_to_strhex(entry->value, &str, true));
                            free(str);
                        }
                        else if (entry->value->type == SML_TYPE_BOOLEAN)
                        {
                            snprintf(tmp_msg, 128, "%d-%d:%d.%d.%d*%d#%s#\n",
                                     entry->obj_name->str[0], entry->obj_name->str[1],
                                     entry->obj_name->str[2], entry->obj_name->str[3],
                                     entry->obj_name->str[4], entry->obj_name->str[5],
                                     entry->value->data.boolean ? "true" : "false");
                        }
                        else if (((entry->value->type & SML_TYPE_FIELD) == SML_TYPE_INTEGER) ||
                                 ((entry->value->type & SML_TYPE_FIELD) == SML_TYPE_UNSIGNED))
                        {
                            double value = sml_value_to_double(entry->value);
                            int scaler = (entry->scaler) ? *entry->scaler : 0;
                            int prec = -scaler;
                            if (prec < 0)
                                prec = 0;
                            value = value * pow(10, scaler);

#ifdef DEBUG
                            snprintf(tmp_msg, 128, "%d-%d:%d.%d.%d*%d#%.*f#",
                                     entry->obj_name->str[0], entry->obj_name->str[1],
                                     entry->obj_name->str[2], entry->obj_name->str[3],
                                     entry->obj_name->str[4], entry->obj_name->str[5], prec, value);
                            Serial.println(tmp_msg);
#endif
                            
                            snprintf(tmp_msg, 128, "%d-%d:%d.%d.%d",
                                     entry->obj_name->str[0], entry->obj_name->str[1],
                                     entry->obj_name->str[2], entry->obj_name->str[3],
                                     entry->obj_name->str[4]);

                            // FILTERING
                            const char *overall_consumption = "1-0:1.8.0";
                            const char *day_consumption = "1-0:1.8.1";
                            const char *night_consumption = "1-0:1.8.2";
                            const char *current_wattage = "1-0:16.7.0";
                            if (!strcmp(overall_consumption, tmp_msg))
                            {
                                snprintf(tmp_msg, 128, "[SML] overall consumption: %f Wh", value);
                                overall_Wh = (float)value;
                            }
                            if (!strcmp(day_consumption, tmp_msg))
                            {
                                snprintf(tmp_msg, 128, "[SML] day consumption: %f Wh", value);
                                day_Wh = (float)value;
                            }
                            if (!strcmp(night_consumption, tmp_msg))
                            {
                                snprintf(tmp_msg, 128, "[SML] night consumption: %f Wh", value);
                                night_Wh = (float)value;
                            }
                            if (!strcmp(current_wattage, tmp_msg))
                            {
                                snprintf(tmp_msg, 128, "[SML] current consumption: %f W", value);
                                curr_W = (float)value;
                            }
                        }
                    }

                    result->current_W = curr_W;
                    result->day_Wh = day_Wh;
                    result->night_Wh = night_Wh;
                    result->overall_Wh = overall_Wh;
                    sml_file_free(file);
                    return 1;
                }
            }

            sml_file_free(file);
        }
        else if (peeked_seq == SML_ESCAPE_SEQ && esc_seq_fir && !esc_seq_sec)
        {
            second_ptr = i;
            esc_seq_sec = TRUE;
        }
        else if (peeked_seq == SML_ESCAPE_SEQ && !esc_seq_fir)
        {
            first_ptr = i;
            esc_seq_fir = TRUE;
        }
    }

    return 0;
}

void ota_setup(void)
{
    // import OTA public key
    Serial.println("[OTA] loading public key from flash...");
    if (!LittleFS.begin())
    {
        Serial.println("[LittleFS] error mouting filesystem.");
        return;
    }
    File ota_public_key_file = File(LittleFS.open("/ota_public.der", "r"));
    if (!ota_public_key_file)
    {
        Serial.println("[LittleFS] error opening OTA public key.");
        return;
    }
    Serial.println("[OTA] successfully loaded public key from flash.");

    // load OTA public key
    ota_public_key = new PublicKey(ota_public_key_file, 
                                    ota_public_key_file.size());
    ota_hash = new HashSHA256();
    ota_signing_verifier = new SigningVerifier(ota_public_key);

    Update.installSignature(ota_hash, ota_signing_verifier);

    LittleFS.end();
}
