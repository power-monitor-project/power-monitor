#include <std.hpp>

/*    NETWORKING CONSTANTS   */
byte mac_addr[] = {0x5C, 0xCF, 0x7F, 0x53, 0xC3, 0x57};
const char *mDNS_name = "power-meter";
const char *host_name = "power-meter.lgn.lan.homenet";

/*    MQTT CONSTANTS   */
const char *mqtt_server = "monitoring.lan.homenet";
const int mqtt_port = 8883;
const char *mqtt_topic_household = "measurement/energy/household";
const char *mqtt_topic_heating = "measurement/energy/heating";
const char *mqtt_topic_debug = "devices";
