#pragma once

/*	INCLUDES	*/
#include <Arduino.h>
#include <SPI.h>
#include <W5500lwIP.h>
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>

/*   	DEFINES   	*/
#define JSON_BUFFER 512
#define RX_BUFFER_SIZE 512
#define SW_RX_HOUSEHOLD D1
#define SW_RX_HEATING D2
#define SML_ESCAPE_SEQ 0x1B1B1B1B
#define SML_ESCAPE_LEN 4
#define FALSE 0x00
#define TRUE 0x01
#define SW_BAUD_RATE 9600
#define ETH_CS_PIN D4
#define NTP_SERVER "de.pool.ntp.org"
#define TZ "CET-1CEST,M3.5.0,M10.5.0/3"

/*    NETWORKING CONSTANTS   */
extern Wiznet5500lwIP *eth_client;
extern byte mac_addr[];
extern const char *mDNS_name;
extern const char *host_name;
extern PublicKey *ota_public_key;
extern HashSHA256 *ota_hash;
extern SigningVerifier *ota_signing_verifier;

/*    MQTT CONSTANTS   */
extern const char *mqtt_server;
extern const int mqtt_port;
extern const char *mqtt_client;
extern const char *mqtt_topic_household;
extern const char *mqtt_topic_heating;
extern const char *mqtt_topic_debug;
