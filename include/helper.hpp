#pragma once
#include <std.hpp>
#include <secrets.hpp>
#include <ringbuffer.h>
#include <sml/sml_file.h>
#include <sml/sml_transport.h>
#include <sml/sml_value.h>
#include <ringbuffer.h>
#include <FastCRC.h>
#include <MQTTPublisher.hpp>
#include <ssl_helper.hpp>
#include <ArduinoOTA.h>

typedef struct
{
    float current_W;
    float overall_Wh;
    float night_Wh;
    float day_Wh;
} sml_data_t;

void network_setup(void);
void chip_setup(void);
void ota_setup(void);

int try_decode_sml(ring_buffer_t *RX_buffer, sml_data_t *result);
