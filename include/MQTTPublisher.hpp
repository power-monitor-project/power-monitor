#pragma once
#include <std.hpp>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <helper.hpp>
#include <WiFiClientSecure.h>

class MQTTPublisher
{
private:
    WiFiClientSecure *wifi_client;
    PubSubClient *pub_sub_client;
    String mqtt_server;
    uint16_t mqtt_server_port;
    void connect(void);

public:
    MQTTPublisher(String mqtt_server, uint16_t port);
    ~MQTTPublisher();
    void publish_household(float curr_W, float overall_Wh);
    void publish_heating(float curr_W, float day_Wh, float night_Wh,
                         float overall_Wh);
    void publish_debug(char *msg, uint16 len);
    void loop(void);
};
