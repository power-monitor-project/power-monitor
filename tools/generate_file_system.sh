#!/bin/bash
openssl x509 -outform der -in certs/pfSense_CA.crt -out data/ca.der
openssl x509 -outform der -in certs/power-meter.crt -out data/client_cert.der
openssl rsa -outform der -in certs/power-meter.key -out data/client_key.der

openssl rsa -outform der -in keys/private.key -out data/ota_public.der -pubout
platformio run --target buildfs --environment nodemcuv
