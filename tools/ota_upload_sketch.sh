#!/bin/bash
OTA_mDNS=$(awk  '/*mDNS_name = /{print substr($5, 2, length($5) - 3)}' "src/std.cpp")
OTA_PASS=$(awk  '/*ota_pass = /{print substr($5, 2, length($5) - 3)}' "src/secrets.cpp")

echo "Connecting to ${OTA_mDNS}.local..."

# upload sketch
echo "Uploading sketch..."
tools/espota-signed --ota-sign-private keys/private.key --upload-built-binary $1 -i ${OTA_mDNS}.local -a ${OTA_PASS} --progress
